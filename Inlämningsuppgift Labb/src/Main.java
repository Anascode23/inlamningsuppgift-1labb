import java.util.Comparator;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Threads implements Runnable {
    @Override
    public void run() {
        System.out.println("Welcome to my thread!");
    }
}
class Person{
    private final String name;
    private final double Salary;
    private final String gender;


    public Person(String name, String gender, double Salary) {
        this.name = name;
        this.gender = gender;
        this.Salary = Salary;
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return Salary;
    }

    public String getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", Salary=" + Salary +
                ", gender='" + gender + '\'' +
                '}';
    }
}

public class Main {
    public static void main(String[] args) throws InterruptedException {


        List<Person> list = List.of(
                new Person("a", "male", 34000),
                new Person("b", "male", 43000),
                new Person("c", "male", 55000),
                new Person("d", "male", 23000),
                new Person("e", "Non-binary", 47000),
                new Person("f", "Non-binary", 45000),
                new Person("g", "female", 30000),
                new Person("h", "female", 60000),
                new Person("i", "female", 58000),
                new Person("j", "female", 61000),
                new Person("k", "female", 44000)
        );

        //1.1
        System.out.println(
                list
                        .stream()
                        .filter(person -> !person.getGender().equals("Non-binary"))
                        .mapToDouble(Person::getSalary)
                        .summaryStatistics()
                        .getAverage()
        );

        //1.2
        System.out.println(
                list
                        .stream()
                        .sorted(Comparator.comparing(Person::getSalary).reversed())
                        .limit(1)
                        .toList()
        );


        //1.3
        System.out.println(
                list
                        .stream()
                        .sorted(Comparator.comparing(Person::getSalary).reversed())
                        .skip(10)
                        .toList()
        );
        //--------------------------------------------------------------

        //2
        CarFactory carFactory = new CarFactory();
        Car create = carFactory.CarFactory(Driving.Good);
        Car create2 = carFactory.CarFactory(Driving.Bad);
        create.drive();
        create2.drive();
        //--------------------------------------------------------------

        //3


        Pattern pattern1 = Pattern.compile("[aeiuyo].*");
        List.of("Hello", "Going", "Murmur" ,"Why" ,"Happy", "hmm", "shh", "tsk")
                .stream()
                .filter(word -> pattern1.matcher(word).find())
                .forEach(System.out::println);


        //-----------------------------------------------------------------
        //4

        Thread thread1 = new Thread(() -> {
            List.of(
                            1,
                            2,
                            3,
                            4,
                            5,
                            6,
                            7,
                            8,
                            9,
                            10,
                            11,
                            12,
                            13,
                            14,
                            15,
                            16,
                            17,
                            18,
                            19,
                            20,
                            21,
                            22,
                            23,
                            24,
                            25,
                            26,
                            27,
                            28,
                            29,
                            30,
                            31,
                            32,
                            33,
                            34,
                            35,
                            36,
                            37,
                            38,
                            39,
                            40,
                            41,
                            42,
                            43,
                            44,
                            45,
                            46,
                            47,
                            48,
                            49,
                            50,
                            51,
                            52,
                            53,
                            54,
                            55,
                            56,
                            57,
                            58,
                            59,
                            60,
                            61,
                            62,
                            63,
                            64,
                            65,
                            66,
                            67,
                            68,
                            69,
                            70,
                            71,
                            72,
                            73,
                            74,
                            75,
                            76,
                            77,
                            78,
                            79,
                            80,
                            81,
                            82,
                            83,
                            84,
                            85,
                            86,
                            87,
                            88,
                            89,
                            90,
                            91,
                            92,
                            93,
                            94,
                            95,
                            96,
                            97,
                            98,
                            99,
                            100,
                            101,
                            102,
                            103,
                            104,
                            105,
                            106,
                            107,
                            108,
                            109,
                            110,
                            111,
                            112,
                            113,
                            114,
                            115,
                            116,
                            117,
                            118,
                            119,
                            120,
                            121,
                            122,
                            123,
                            124,
                            125,
                            126,
                            127,
                            128,
                            129,
                            130,
                            131,
                            132,
                            133,
                            134,
                            135,
                            136,
                            137,
                            138,
                            139,
                            140,
                            141,
                            142,
                            143,
                            144,
                            145,
                            146,
                            147,
                            148,
                            149,
                            150,
                            151,
                            152,
                            153,
                            154,
                            155,
                            156,
                            157,
                            158,
                            159,
                            160,
                            161,
                            162,
                            163,
                            164,
                            165,
                            166,
                            167,
                            168,
                            169,
                            170,
                            171,
                            172,
                            173,
                            174,
                            175,
                            176,
                            177,
                            178,
                            179,
                            180,
                            181,
                            182,
                            183,
                            184,
                            185,
                            186,
                            187,
                            188,
                            189,
                            190,
                            191,
                            192,
                            193,
                            194,
                            195,
                            196,
                            197,
                            198,
                            199,
                            200,
                            201,
                            202,
                            203,
                            204,
                            205,
                            206,
                            207,
                            208,
                            209,
                            210,
                            211,
                            212,
                            213,
                            214,
                            215,
                            216,
                            217,
                            218,
                            219,
                            220,
                            221,
                            222,
                            223,
                            224,
                            225,
                            226,
                            227,
                            228,
                            229,
                            230,
                            231,
                            232,
                            233,
                            234,
                            235,
                            236,
                            237,
                            238,
                            239,
                            240,
                            241,
                            242,
                            243,
                            244,
                            245,
                            246,
                            247,
                            248,
                            249,
                            250
                    )
                    .forEach(x -> System.out.println(x + " from thread 1"));
        });


        Thread thread2 = new Thread(() -> {
            List.of(251, 252, 253, 254, 255, 256, 257, 258,
                            259,
                            260,
                            261,
                            262,
                            263,
                            264,
                            265,
                            266,
                            267,
                            268,
                            269,
                            270,
                            271,
                            272,
                            273,
                            274,
                            275,
                            276,
                            277,
                            278,
                            279,
                            280,
                            281,
                            282,
                            283,
                            284,
                            285,
                            286,
                            287,
                            288,
                            289,
                            290,
                            291,
                            292,
                            293,
                            294,
                            295,
                            296,
                            297,
                            298,
                            299,
                            300,
                            301,
                            302,
                            303,
                            304,
                            305,
                            306,
                            307,
                            308,
                            309,
                            310,
                            311,
                            312,
                            313,
                            314,
                            315,
                            316,
                            317,
                            318,
                            319,
                            320,
                            321,
                            322,
                            323,
                            324,
                            325,
                            326,
                            327,
                            328,
                            329,
                            330,
                            331,
                            332,
                            333,
                            334,
                            335,
                            336,
                            337,
                            338,
                            339,
                            340,
                            341,
                            342,
                            343,
                            344,
                            345,
                            346,
                            347,
                            348,
                            349,
                            350,
                            351,
                            352,
                            353,
                            354,
                            355,
                            356,
                            357,
                            358,
                            359,
                            360,
                            361,
                            362,
                            363,
                            364,
                            365,
                            366,
                            367,
                            368,
                            369,
                            370,
                            371,
                            372,
                            373,
                            374,
                            375,
                            376,
                            377,
                            378,
                            379,
                            380,
                            381,
                            382,
                            383,
                            384,
                            385,
                            386,
                            387,
                            388,
                            389,
                            390,
                            391,
                            392,
                            393,
                            394,
                            395,
                            396,
                            397,
                            398,
                            399,
                            400,
                            401,
                            402,
                            403,
                            404,
                            405,
                            406,
                            407,
                            408,
                            409,
                            410,
                            411,
                            412,
                            413,
                            414,
                            415,
                            416,
                            417,
                            418,
                            419,
                            420,
                            421,
                            422,
                            423,
                            424,
                            425,
                            426,
                            427,
                            428,
                            429,
                            430,
                            431,
                            432,
                            433,
                            434,
                            435,
                            436,
                            437,
                            438,
                            439,
                            440,
                            441,
                            442,
                            443,
                            444,
                            445,
                            446,
                            447,
                            448,
                            449,
                            450,
                            451,
                            452,
                            453,
                            454,
                            455,
                            456,
                            457,
                            458, 459, 460, 461, 462, 463,
                            464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484,
                            485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500)
                    .forEach(x -> System.out.println(x + " from thread 2"));
        });

        thread1.start();
        thread1.join();
        thread2.start();
        thread2.join();

    }
}
